package main

import (
	"fmt"
	"net/http"
)

// HelloWorld function
func HelloWorld(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "Argocd demo")
}

func main() {
	http.HandleFunc("/", HelloWorld)
	fmt.Println("App is listening on port 3000")
	http.ListenAndServe(":3000", nil)
}
