# Multi stage go build
# Golang unit test and build
FROM golang:1.15.2
MAINTAINER laxmi@gmail.com
RUN mkdir /app
ADD . /app
WORKDIR /app
# Running unit test
RUN go test -v
# Build app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .


# Copy the binary from old build and create app
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
RUN mkdir /app
WORKDIR /app
COPY --from=0 /app .
CMD ["./main"]  
